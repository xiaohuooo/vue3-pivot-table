import { createApp } from "vue";
import "element-plus/dist/index.css";
import App from "./App.vue";
import * as echarts from "echarts";
createApp(App).use(echarts).mount("#app");
